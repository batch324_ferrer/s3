<?php 

// $hello = "Hello World";

$buildingObj = (object)[
	'name' 		=> 'Caswynn Building',
	'floors' 	=> 8,
	'address' 	=> (object)[
		'barangay' 	=> 'Sacred Heart',
		'city' 		=> 'Quezon City',
		'country' 	=> ' Philippines'
	]

];


//creating an object using a Class
class Building {
	//A constructor is used during the creation of an object (double underscore __)

	public function __construct($name, $floors, $address, $zipCode){
		$this->name = $name;
		$this->floors = $floors;
		$this->address = $address;
		$this->zipCode = $zipCode;
	}

	public function printName(){
		return "The name of the building is $this->name";
	}

	public function checkFloors(){
		return "$this->floors";
	}

	public function checkZipCode(){
		return "zipCode is $this->zipCode.";
	}

};

$building = new Building ('Caswynn Building', 8,'Sacred Heart Quezon City', 0001);
$secondBuilding = new Building ('Trial Building', 100,'Somewhere', 0002);



class Condominium extends Building {
	public $room;
	public function __construct($name, $floors, $address, $zipCode, $rooms){
		$this->name 	= $name;
		$this->floors 	= $floors;
		$this->address 	= $address;
		$this->zipCode 	= $zipCode;
		$this->rooms 	= $rooms;
	}
	public function printName(){
		return "The name of the Condominium is $this->name.";
	}
	public function checkFloors(){
		return "$this->floors with $this->rooms";
	}

};

$condominium = new Condominium ('Trial Condominium', 50, 'Manila City, Manila', 0003, 500);

//Abstraction
abstract class Drink{
	public $name;

	public function __construct($name){
		$this->name 	= $name;		
	}

	public abstract function getDrinkName();

}


class Coffee extends Drink{
	public function getDrinkName(){
		return "The name of the coffee is $this->name.";
	}
}


$kopiko = new Coffee ('Kopiko');































?>